from collections import Counter

def score_poker_hands(hand1, hand2):
    """
    Given two poker hands, represented by strings, return an integer result:
        
        0: No winner, Tie ==> return 0
        1: Hand 1 Wins ==> return 1
        2: Hand 2 Wins ==> return 2
        
        the use of hand in the compare numbers function is temporary.

    """
    if not hand1 or not hand2:
        raise ValueError("Invalid input")

    hand1_arr_string = hand_to_card(hand1) 
    hand2_arr_string = hand_to_card(hand2)

    # '5S 3C JC' => 5, 3, 11 => [5, 3, 11]
    hand1_arr_ranks = [parse_card(card_string) for card_string in hand1_arr_string] 
    hand2_arr_ranks = [parse_card(card_string) for card_string in hand2_arr_string] 

    # check both hands for trips
    hand1_trips_ranks = check_for_trips(hand1_arr_ranks) #[]
    hand2_trips_ranks = check_for_trips(hand2_arr_ranks)

    if hand1_trips_ranks > hand2_trips_ranks:
        return 1
    elif hand2_trips_ranks > hand1_trips_ranks:
        return 2

    # check both hands for pair
    hand1_pairs_ranks = check_for_pair(hand1_arr_ranks) #[]
    hand2_pairs_ranks = check_for_pair(hand2_arr_ranks)
    
    if hand1_pairs_ranks and not hand2_pairs_ranks:
        return 1
    if not hand1_pairs_ranks and hand2_pairs_ranks:
        return 2

    # nested logic for if both have pair
    if hand1_pairs_ranks and hand2_pairs_ranks:
        if hand1_pairs_ranks > hand2_pairs_ranks:
            return 1
        elif hand2_pairs_ranks > hand1_pairs_ranks:
            return 2
        else:
            # check for highest card after the pair
            return find_highest_card(hand1_arr_ranks, hand2_arr_ranks)

    # next we need to test for the higher winning pair


    # check for highest rank
    return find_highest_card(hand1_arr_ranks, hand2_arr_ranks)


    # FIXME: Implement Poker scoring here, I guess...
    pass
    # TODO: Figure out how Poker hands are supposed to be scored?

def find_highest_card(hand1_arr, hand2_arr):
    """
        Takes two array of hand values
        Sorts both arrays
        Then compare the arrays
        hand_arr 1 > hand_arr 2 return 1
        hand_arr 2 > hand_arr 1 return 2
        hand_arr 2 == hand_arr 1 return 0
    """
    hand1_arr.sort(reverse=True)
    hand2_arr.sort(reverse=True)

    if hand1_arr > hand2_arr:
        return 1
    elif hand2_arr > hand1_arr:
        return 2
    else:
        return 0



def check_for_pair(array_of_numbers):
    """
        Takes a single hand
        Returns an array of ranks that have pairs
        if no pairs : []
        if one pair :[9]
        if two pairs : [9,10]
    """
    # array_of_numbers = [parse_card(card_string) for card_string in hand]
    duplicate_numbers = Counter(array_of_numbers)
    paired_ranks = []

    for rank, count in duplicate_numbers.items():
        if count == 2:
            paired_ranks.append(rank)
            
    
    paired_ranks.sort(reverse=True)
    return paired_ranks

def check_for_trips(array_of_numbers):
    """
        Takes a single hand
        Returns an array of ranks that have trips
        if no trips : []
        if trips :[9]
    """
    duplicate_numbers = Counter(array_of_numbers)
    trips_rank = []

    for rank, count in duplicate_numbers.items():
        if count == 3:
            trips_rank.append(rank)

    return trips_rank

def parse_card(card_string):
    """
    takes a card string such as "4H" 
    returns an integer with the numeric rank "4"

    Converts face cards to their numerical equivalent

    returns Value Errors for:
        empty or none inputs
        bad numbers (input < 2 or >10)
        bad strings (not a suit)

    """

    if not card_string:
        raise ValueError("Invalid input")

    card_rank = card_string[:-1]
    card_suit = card_string[-1]

    valid_suits = {
        "C","D","H","S"
    }

    if not card_suit in valid_suits:
        raise ValueError("Invalid suit.")

    face_card_conversion = {
        "T": 10,
        "J": 11,
        "Q": 12,
        "K": 13,
        "A": 14
    }

    if card_rank.isdigit():
        if not (int(card_rank) >= 2 and int(card_rank) <= 9):
            raise ValueError("Invalid rank.")

    # check is it a digit first
    if not card_rank.isdigit():
        # is it a valid face_card string?
        if card_rank not in face_card_conversion:
            raise ValueError("Invalid rank.")
        
        card_rank = face_card_conversion[card_rank]

    return int(card_rank)

def hand_to_card(hand):
    """
        Accepts string with spaces between string representations of cards. 
        Returns an array of the individual card strings.
    
    """

    return hand.split(" ")


# class PokerError(message):
#     """
#         Create custom error messages by creating messages as class instances.
#         To call use "raise pokerError("error message")
#         Extensive discussion of the potential issues of this implementation:
#         https://stackoverflow.com/questions/1319615/proper-way-to-declare-custom-exceptions-in-modern-python
#     """
#     pass

# def comparing_numbers(hand_of_cards1, hand_of_cards2):
#     """

#         Takes two cards as arrays
#         Parses card strings into integers (ranks)
#         Then compares their values
#         card 1 > card 2 return 1
#         card 2 > card 1 return 2
#         card 2 == card 1 return 0

#     """
    
    # rank_1 = max(hand_of_cards1)
    # rank_2 = max(hand_of_cards2)

    # if rank_1 > rank_2:
    #     return 1
    # elif rank_1 < rank_2:
    #     return 2
    # else:
    #     return 0