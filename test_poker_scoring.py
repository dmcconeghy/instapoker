from poker_scoring_api import score_poker_hands, parse_card, hand_to_card, check_for_pair, find_highest_card, check_for_trips

import pytest

def test_example_unit_test():
    # FIXME: Maybe assert something meaningful about score_poker_hands?
    assert score_poker_hands is not None

def test_return_is_integer():
    return_val = score_poker_hands("2D", "6H")
    assert isinstance(return_val, int)

def test_return_valid_response():
    return_val = score_poker_hands("2D", "6H")
    valid_response = [0,1,2]
    assert return_val in valid_response

def test_numeric_comparison():
    return_val = score_poker_hands("9H", "8S")
    assert return_val == 1
    return_val = score_poker_hands("8H", "9S")
    assert return_val == 2
    return_val = score_poker_hands("5H", "5S")
    assert return_val == 0

def test_face_card_comparison():
    return_val = score_poker_hands( 'JC', 'QD')
    assert return_val == 2
    return_val = score_poker_hands( 'AC', 'QD')
    assert return_val == 1
    return_val = score_poker_hands( 'AC', 'AD')
    assert return_val == 0


def test_number_card_face_card_comparison():
    return_val = score_poker_hands( '2C', 'KD')
    assert return_val == 2


def test_comparing_numbers():
    card1, card2 = [9], [2]
    results = find_highest_card(card1, card2) 
    print("\nResults: ", results)
    assert results == 1
    
def test_parse_card():
    assert parse_card("2H") == 2

def test_invalid_suit():
    with pytest.raises(ValueError):
        assert parse_card("2F")

def test_invalid_rank():
    with pytest.raises(ValueError):
        assert parse_card("ZD") 
    with pytest.raises(ValueError):
        assert parse_card("10H")

def test_invalid_integer():
    with pytest.raises(ValueError):
        assert parse_card("11D") 

def test_empty_input():
    with pytest.raises(ValueError):
        assert parse_card("")
    with pytest.raises(ValueError):
        assert parse_card(None)

def test_multi_card_hand():
    return_val = hand_to_card( 'JC 5S 3C')
    assert return_val == ['JC', '5S', '3C']

def test_compare_hands():
    return_val = score_poker_hands('5S 3C JC', '6H 9D TC')
    assert return_val == 1
    return_val = score_poker_hands('8H 6D', 'KS QH')
    assert return_val == 2
    return_val = score_poker_hands('AS 4H', 'AH 2D')
    assert return_val == 1


@pytest.mark.parametrize('rank_array, result', [
    ([8, 6, 9], []),
    ([8, 3, 2, 8], [8]),
    ([3,8,3,8], [8,3])
])
def test_check_for_pair(rank_array, result):
    return_val = check_for_pair(rank_array)
    assert return_val == result

# test for hand1 has pair hand2 doesnt + inverse

def test_hands_for_winning_pair():
    # hand1 has pair, hand2 doesn't
    return_val = score_poker_hands('5S 5C 3C', '2H 3D 4C')
    assert return_val == 1
    # hand1 doesn't have pair, hand2 does
    return_val = score_poker_hands('2H 4D 3C', '5S 5C 4C')
    assert return_val == 2
    # both hands have pairs

def test_hands_if_both_have_pairs():
    #value of pairs are tied with different kickers
    return_val = score_poker_hands('5H 5D 4C', '5S 5C 3C')
    assert return_val == 1
    #value of pairs are different and first hand is higher
    return_val = score_poker_hands('5H 5D 4C', '4S 4C 3C')
    assert return_val == 1
    #value of pairs are different and second hand is higher
    return_val = score_poker_hands('6H 6D 4C', '7S 7C 3C')
    assert return_val == 2
    #value of pairs are the same and kicker is the same
    return_val = score_poker_hands('5H 5D 4C', '5S 5C 4D')
    assert return_val == 0

def test_multi_pair_hands():
    #value of pairs are tied with different kickers
    return_val = score_poker_hands('5H 7D 5C 7H TC', '5D 5S 7S 3H 7C')
    assert return_val == 1
    #value of pairs are different and first hand is higher
    return_val = score_poker_hands('5H 5D 6H 6C 4C', '4S 4C 3C 6S 6D')
    assert return_val == 1
    #value of pairs are different and second hand is higher
    return_val = score_poker_hands('6H 6D 4C 5H 5D', '7S 7C 3C 4S 4D')
    assert return_val == 2
    #value of pairs are the same and kicker is the same
    return_val = score_poker_hands('3C 7H 5C 5H 7C', '5D 5S 7S 3H 7D')
    assert return_val == 0

def test_check_for_trips():
    return_val = check_for_trips([4, 4, 4, 7])
    assert return_val == [4]
    return_val = check_for_trips([4,2,4,3])
    assert return_val == []

def test_trips_hands():
    #Trips beat weaker hands
    return_val = score_poker_hands(
        '5H 5D 5C 7H TC',
        '2D 4H 2H 4D 6S'
        )
    assert return_val == 1
    #Both hands are trips, hand 1 wins
    return_val = score_poker_hands(
        '5H 5D 5C 7H TC',
        '4S 4C 4H 6S 9D'
        )
    assert return_val == 1
    #Both hands are trips, hand 2 wins
    return_val = score_poker_hands(
        '4S 4C 4H 6S 9D',
        '5H 5D 5C 7H TC'
        )
    assert return_val == 2
